import React, { useState } from 'react';
import './App.css';

function App() {
  const [chatInput, setChatInput] = useState('');

  function handleSend() {
    const existingChats = JSON.parse(localStorage.getItem("chats") || '[]');
    existingChats.push(chatInput);
    localStorage.setItem("chats", JSON.stringify(existingChats));
  }

  function handleShowMessages() {
    const chats = JSON.parse(localStorage.getItem("chats") || '[]');
    console.log("Chats:", chats);
  }

  return (
    <div className="chat-app">
      <h1>w14 Chat app to localStorage</h1>
      <div className='inputs'>
      <input 
        type="text" 
        id="chat" 
        value={chatInput}
        onChange={(e) => setChatInput(e.target.value)}
      />
      <button onClick={handleSend}>Send</button>
      <button onClick={handleShowMessages}>Show messages</button>
      </div>
      <div className='messages'>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tellus rutrum tellus pellentesque eu tincidunt. Sed ullamcorper morbi tincidunt ornare massa. In dictum non consectetur a erat nam. Turpis cursus in hac habitasse platea dictumst quisque sagittis. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus iaculis. Scelerisque purus semper eget duis. Mi tempus imperdiet nulla malesuada pellentesque. Neque gravida in fermentum et sollicitudin ac orci phasellus. Sed tempus urna et pharetra pharetra massa massa. Ut venenatis tellus in metus. Maecenas accumsan lacus vel facilisis volutpat est. Vitae proin sagittis nisl rhoncus. Sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Commodo odio aenean sed adipiscing diam donec adipiscing tristique risus. At tempor commodo ullamcorper a lacus vestibulum sed arcu non. Volutpat diam ut venenatis tellus in metus vulputate eu scelerisque.</p>
        <p>In eu mi bibendum neque. Ac turpis egestas sed tempus urna. Ultrices vitae auctor eu augue. Risus sed vulputate odio ut. Et malesuada fames ac turpis egestas. Tincidunt arcu non sodales neque sodales ut. Amet dictum sit amet justo donec. Scelerisque in dictum non consectetur a erat nam. Massa tincidunt dui ut ornare lectus sit amet. Sem viverra aliquet eget sit amet tellus. Imperdiet sed euismod nisi porta. Sit amet consectetur adipiscing elit pellentesque habitant morbi. Lectus vestibulum mattis ullamcorper velit. Felis donec et odio pellentesque diam volutpat commodo. Scelerisque in dictum non consectetur a. Tellus in hac habitasse platea dictumst vestibulum. Vitae et leo duis ut diam. Sed cras ornare arcu dui vivamus arcu felis bibendum ut. Accumsan tortor posuere ac ut consequat semper viverra nam.</p>
        <p>Ornare quam viverra orci sagittis eu volutpat odio facilisis mauris. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit. Tincidunt praesent semper feugiat nibh sed pulvinar proin gravida hendrerit. Gravida arcu ac tortor dignissim convallis. Cursus vitae congue mauris rhoncus aenean. Dui vivamus arcu felis bibendum ut tristique et. In ornare quam viverra orci. Euismod nisi porta lorem mollis aliquam ut porttitor leo. Laoreet non curabitur gravida arcu ac tortor. Arcu bibendum at varius vel pharetra.</p>
        <p>Ultrices in iaculis nunc sed augue lacus. Pharetra vel turpis nunc eget lorem dolor. Tincidunt tortor aliquam nulla facilisi cras fermentum odio eu feugiat. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Convallis convallis tellus id interdum velit. Eu non diam phasellus vestibulum lorem sed risus ultricies tristique. Eu feugiat pretium nibh ipsum consequat nisl vel. Mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar. Sem et tortor consequat id porta nibh venenatis cras sed. Facilisis volutpat est velit egestas dui id ornare arcu odio. Imperdiet sed euismod nisi porta. Tortor posuere ac ut consequat semper. Id cursus metus aliquam eleifend mi in nulla posuere. Nulla facilisi morbi tempus iaculis urna. Faucibus a pellentesque sit amet porttitor eget. Enim ut tellus elementum sagittis vitae et. Elit eget gravida cum sociis natoque. Sapien nec sagittis aliquam malesuada. Ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis.</p>
        <p>Non blandit massa enim nec dui nunc mattis. Interdum velit euismod in pellentesque massa placerat. Nullam non nisi est sit amet facilisis magna. Sed libero enim sed faucibus turpis in. Suscipit adipiscing bibendum est ultricies integer quis. Sed adipiscing diam donec adipiscing tristique risus nec. Condimentum id venenatis a condimentum vitae sapien pellentesque. Tortor at risus viverra adipiscing at in tellus integer feugiat. Dolor purus non enim praesent elementum facilisis. Fringilla est ullamcorper eget nulla. Gravida dictum fusce ut placerat. Lorem donec massa sapien faucibus et. Venenatis tellus in metus vulputate.</p>
        </div>
    </div>
  );
}

export default App;
